<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCltTiposCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('clt_tipos_cuentas');
        Schema::create('clt_tipos_cuentas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('banco_id')->comment('Referenciado a clt_bancos');
            $table->unsignedInteger('id_tipo_cuenta')->comment('Tipo de cuenta del banco. Referenciado a com_subtipos');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('banco_id')->references('id')->on('clt_bancos');
            $table->foreign('id_tipo_cuenta')->references('id')->on('com_subtipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clt_tipos_cuentas');
    }
}
