<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdmConfiguracionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_configuraciones', function (Blueprint $table) {
            if(!Schema::hasColumn('adm_configuraciones', 'nombre_empresa')) {
                $table->string('nombre_empresa',100)
                ->comment('Nombre de la empresa');
            }
            if(!Schema::hasColumn('adm_configuraciones', 'logo')) {
                $table->string('logo',100)
                ->comment('Logo de la empresa');
            }
            if(!Schema::hasColumn('adm_configuraciones', 'inactividad')) {
                $table->boolean('inactividad')
                ->comment('Marca que indica si el sistema se encuentra inactivo');
            }
            if(!Schema::hasColumn('adm_configuraciones', 'motivo_inactividad')) {
                $table->text('motivo_inactividad')
                ->comment('Mensaje que indica el motivo de inactividad inactivo');
            }
            if(!Schema::hasColumn('adm_configuraciones', 'notificacion')) {
                $table->text('notificacion')
                ->comment('Notificación otorgada al cliente');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_configuraciones', function (Blueprint $table) {
            if(Schema::hasColumn('adm_configuraciones', 'nombre_empresa')) {
                $table->dropColumn('nombre_empresa');
            }
            if(Schema::hasColumn('adm_configuraciones', 'logo')) {
                $table->dropColumn('logo');
            }
            if(!Schema::hasColumn('adm_configuraciones', 'inactividad')) {
                $table->dropColumn('inactividad');
            }
            if(Schema::hasColumn('adm_configuraciones', 'motivo_inactividad')) {
                $table->dropColumn('motivo_inactividad');
            }
            if(Schema::hasColumn('adm_configuraciones', 'notificacion')) {
                $table->dropColumn('notificacion');
            }
        });
    }
}
