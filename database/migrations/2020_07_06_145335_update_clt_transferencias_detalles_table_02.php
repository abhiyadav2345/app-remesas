<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCltTransferenciasDetallesTable02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clt_transferencias_detalles', function (Blueprint $table) {
            if(Schema::hasColumn('clt_transferencias_detalles', 'monto_envio')) {
                $table->string('monto_envio', 255)->change();
            }
            if(Schema::hasColumn('clt_transferencias_detalles', 'monto_recibido')) {
                $table->string('monto_recibido', 255)->change();
            }
            if(Schema::hasColumn('clt_transferencias_detalles', 'numero_cuenta')) {
                $table->string('numero_cuenta', 50)->change();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clt_transferencias_detalles', function (Blueprint $table) {
            if(Schema::hasColumn('clt_transferencias_detalles', 'monto_envio')) {
                $table->decimal('monto_envio', 20, 2)->change();
            }
            if(Schema::hasColumn('clt_transferencias_detalles', 'monto_recibido')) {
                $table->decimal('monto_recibido', 20, 2)->change();
            }
            if(Schema::hasColumn('clt_transferencias_detalles', 'numero_cuenta')) {
                $table->integer('numero_cuenta')->change();
            }
        });
    }
}
