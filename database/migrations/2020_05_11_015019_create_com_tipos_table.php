<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComTiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('com_tipos');
        Schema::create('com_tipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 80)->comment('Nombre del tipo');
            $table->string('apodo', 80)->comment('Apodo del tipo en sistema');

            $table->string('descripcion')
            ->nullable(true)
            ->comment('Descripción breve del significado del tipo');

            $table->timestamps();
            $table->softDeletes();

            $table->index('apodo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('com_tipos');
    }
}
