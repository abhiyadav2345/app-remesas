<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCltRechazosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clt_rechazos', function (Blueprint $table) {
            if(Schema::hasColumn('clt_rechazos', 'transferencia_id')) {
                if($table->hasForeign(['transferencia_id'])) {
                    $table->dropForeign(['transferencia_id']);
                }
                $table->dropColumn('transferencia_id');
            }
            if(!Schema::hasColumn('clt_rechazos', 'transferencia_detalle_id')) {
                $table->unsignedBigInteger('transferencia_detalle_id')
                ->after('id')
                ->comment('Refenciado a clt_transferencias_detalles');

                if(!$table->hasForeign(['transferencia_detalle_id'])){
                    $table->foreign('transferencia_detalle_id')
                    ->references('id')
                    ->on('clt_transferencias_detalles');
                }

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clt_rechazos', function (Blueprint $table) {
            if(!Schema::hasColumn('clt_rechazos', 'transferencia_id')) {
                $table->unsignedBigInteger('transferencia_id')
                ->after('id')
                ->comment('Refenciado a clt_transferencias');

                if(!$table->hasForeign(['transferencia_id'])) {
                    $table->foreign('transferencia_detalle_id')
                    ->references('id')
                    ->on('clt_transferencias_detalles');
                }
            }
            if(Schema::hasColumn('clt_rechazos', 'transferencia_detalle_id')) {
                $table->unsignedBigInteger('transferencia_detalle_id')
                ->comment('Refenciado a clt_transferencias_detalles');

                if($table->hasForeign(['transferencia_detalle_id'])) {
                    $table->dropForeign(['transferencia_detalle_id']);
                }
            }
        });
    }
}
