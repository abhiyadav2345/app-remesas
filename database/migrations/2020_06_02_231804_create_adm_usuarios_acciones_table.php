<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmUsuariosAccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('adm_usuarios_acciones');
        Schema::create('adm_usuarios_acciones', function (Blueprint $table) {
            $table->unsignedInteger('usuario_id')->comment('Usuario administrador. Referencia a adm_usuarios');
            $table->unsignedInteger('accion_id')->comment('Acción de usuario administrador. Referencia a adm_accion');
            $table->timestamp('created_at')->useCurrent();

            $table->primary(['usuario_id', 'accion_id']);

            $table->foreign('usuario_id')
            ->references('id')
            ->on('adm_usuarios')
            ->onDelete('cascade');

            $table->foreign('accion_id')
            ->references('id')
            ->on('adm_acciones')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_usuarios_acciones');
    }
}
