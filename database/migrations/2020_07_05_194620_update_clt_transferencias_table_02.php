<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCltTransferenciasTable02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clt_transferencias', function(Blueprint $table){
            if(!Schema::hasColumn('clt_transferencias', 'pais_destino_id')) {
                $table->unsignedInteger('pais_destino_id')
                ->after('tasa')
                ->comment('País de origen de transferencia');
            }
            if(!Schema::hasColumn('clt_transferencias', 'pais_origen_id')) {
                $table->unsignedInteger('pais_origen_id')
                ->after('tasa')
                ->comment('País de destino de transferencia');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clt_transferencias', function(Blueprint $table){
            if(Schema::hasColumn('clt_transferencias', 'pais_destino_id')) {
                $table->dropColumn('pais_destino_id');
            }
            if(Schema::hasColumn('clt_transferencias','pais_origen_id')){
                $table->dropColumn('pais_origen_id');
            }
        });
    }
}
