<?php

// RUTAS SIN PERMISOS
Route::namespace('Cliente')->prefix('cliente')->name('cliente')->group(function() {

});

// RUTAS CON PERMISOS
Route::middleware('auth')->group(function() {
	Route::namespace('Cliente')->prefix('cliente')->name('cliente')->group(function() {

		// TRANSFERENCIAS
		Route::prefix('transferencias')->name('.transferencias')->group(function() {
			Route::get('/', 'TransferenciaDetalleController@vue');
			Route::post('/', 'TransferenciaDetalleController@index');
			Route::post('detalles', 'TransferenciaDetalleController@detalles')->name('detalles');
			Route::post('aceptar', 'TransferenciaDetalleController@aceptar')->name('aceptar');
			Route::post('rechazar', 'TransferenciaDetalleController@rechazar')->name('rechazar');
			Route::post('finalizar', 'TransferenciaDetalleController@finalizar')->name('finalizar');
			Route::post('contactar_soporte', 'TransferenciaDetalleController@contactar_soporte')
			->name('contactar_soporte');
		});

	});
	
});
