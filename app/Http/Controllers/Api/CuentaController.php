<?php

namespace App\Http\Controllers\Api;

use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Cliente\Banco;
use App\Models\Cliente\Cuenta;
use App\Http\Controllers\Controller;

class CuentaController extends Controller
{
    public function index ( Request $request )
    {
        $cuenta = Cuenta::where('usuario_id',Auth::user()->id)
            ->with('_banco._tipos' , '_tipo_cuenta')
            ->paginate();

        $bancos = Banco::with('_tipos')->get();

        return response([
            'rows' => $cuenta,
            'bancos'=>$bancos,
        ],200);
    }

    public function registrar ( Request $request )
    {
        try {
            DB::beginTransaction();

            $validator = $this->validar($request->all(), [
                'razon_social' => 'string|max:150|required|max:250',
                'cedula' => 'integer|required|min:6',
                'numero_cuenta' => 'integer|required|digits_between:8,30',
                'banco_id' => 'integer|required',
                'id_tipo_cuenta' => 'integer|required'
            ]);

            if(!$validator->status ){
                return response(['errors' => $validator->errors],422);
            }

            $request->merge(['usuario_id' => Auth::user()->id ]);

            if($request->id){
                $cuenta = Cuenta::find($request->id);
                $cuenta->fill($request->all());
                $cuenta->save();
            }else{
                $cuenta = Cuenta::create($request->all());
            }

            $cuentas = Cuenta::where('usuario_id',Auth::user()->id)
                ->with('_banco')
                ->paginate();

            DB::commit();
            return response([
                'succes' => 'Operación realiza con éxito',
                'cuentas' => $cuentas,
            ]);

        } catch (\Exception $e) {

            DB::rollback();
            return  response([
                'error' => $e->getMessage()
            ]);
        }
    }

    public function delete (Request $request)
    {
        try {
            DB::beginTransaction();

            $validator = $this->validar($request->all(), [
                'cuenta_id' => 'required|exists:clt_cuentas,id',
            ]);

            if(!$validator->status){
                return response(['errors' => $validator->errors ],422);
            }

            Cuenta::findOrFail($request->cuenta_id)->delete();
            $cuentas = Cuenta::where('usuario_id',Auth::user()->id)
                ->with('_banco')
                ->paginate();

            DB::commit();

            return response([
                'succes' => 'Operación realizada con éxito.',
                'cuentas'=>$cuentas
            ]);

        } catch (\Exception $e) {

            DB::rollback();

            return  response([
                'error' => $e->getMessage()
            ],500);
        }

    }
}
