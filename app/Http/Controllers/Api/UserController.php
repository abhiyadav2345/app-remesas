<?php
namespace App\Http\Controllers\Api;

use DB;
use Auth;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Cliente\Usuario;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Notifications\NotificationUser;
use App\Models\Cliente\PasswordRecovery;
class UserController extends Controller
{

    public function register( Request $request )
    {
        $data = $request->all();

        $validacion = Validator::make($data,[
            'nombre' => 'required|string|max:150',
            'apellidos' => 'required|string|max:150',
            'email' => 'required|email:rfc,dns|unique:clt_usuarios',
            'password' => 'required|min:8|max:30',
            'password_validacion' => 'required|same:password',
        ]);

        if($validacion->fails()){
            return response([
                'errors' => $validacion->errors()
            ],422);
        }

        try {

            $data['password'] = Hash::make($request->password);
            // creacion del usuario
            $user = Usuario::create($data);
            // creacion del token
            $token = $user->createToken('remesa_token')->accessToken;

            return response([
                'succes' => 'Operación realizada con éxito.',
                'remesa_token'=>$token,
            ],200);

        } catch (\Exception $e) {

            return response([
                'error' => $e->getMessage(),
            ],500);
        }
    }

    public function login( Request $request )
    {
        $validacion = Validator::make($request->all(),[
            'email' => 'required|email:rfc,dns',
            'password' => 'required|min:8|max:30',
        ]);

        if($validacion->fails()){
            return response([
                'errors' => $validacion->errors()
            ],419);
        }

        try {

            // $user =  Auth::guard('apiweb')->attempt($request->all());
            $user = Usuario::where('email', $request->email)->first();

            if(!$user ||  !Hash::check($request->password, $user->password)){
                throw new \Exception("Usuario o Contraseña invalida.");
            }

            // creacion del token
            $token = $user->createToken('remesa_token')->accessToken;

            return response([
                'succes' => 'Operación realizada con éxito.',
                'remesa_token' => $token,
            ],200);

        } catch (\Exception $e) {

            return response([
                'error' => $e->getMessage(),
            ],401);
        }
    }

    public function logout (Request $request)
    {
        try {
            if(!Auth::user()) throw new \Exception("");

            Auth::user()->tokens()->each(function($token){
                $token->delete();
            });

            return response([
                'succes' => 'Operación realizada con éxito.',
            ],200);

        } catch (\Exception $e) {

            return response([
                'error' => 'Lo sentimos un error ha ocurrido.',
            ],500);
        }
    }

    public function user(Request $request)
    {
        return response(['user'=>Auth::user()],200);
    }

    public function recovery(Request $request)
    {
        $data = $request->all();
        $validacion = Validator::make($data,[
            'email' => 'required|email:rfc,dns'
        ]);
        
        if($validacion->fails()){
            return response([
                'errors' => $validacion->errors()
            ],422);
        }

        try{
            DB::beginTransaction(); 
            $fecha = date('Y-m-d H:i:s');
            
            //buscar al usuario
            $user = Usuario::where('email', $request->email)
            ->with('_recovery')
            ->first();

            //verificar sio tiene algun token pendientre
            if(!empty($user->_recovery) && $user->_recovery->expires_at->gt($fecha) ){
                throw new \Exception('Ya cuenta con una verificacion pendiente.');

            };
            //guardar token
            $recovery = new PasswordRecovery;
            $recovery->usuario_id = $user->id;
            $recovery->email = $user->email;
            $recovery->token = Hash::make( $user->email.time() );
            $recovery->expires_at = Carbon::parse($fecha)->addDays(1)->format('Y-m-d H:i:s');
            $recovery->save();
            $mail = $this->enviarNotificacion($user, $recovery);

            if(!$mail) throw new \Exception('Un error ha ocurrido, porfavor vuelva a intentarlo mas tarde.');
            
            DB::commit(); 
            return response([
                'success' => 'Operacion realizada con exito, porfavor Revise su correo.'
            ],200);

        }catch(\Exception $e){
            DB::rollback();
            return response([
                'error'=>$e->getMessage()
            ],500);
        }
    }

    public function recoveryEmail(Request $request)
    {
        $data = $request->all();

        $validacion = Validator::make($data,[
            'password' => 'required|min:8|max:30',
            'password_validacion' => 'required|same:password',
            'email' => 'required|string',
            'hash' => 'required|string',
        ]);
        
        if($validacion->fails()){
            return response([
                'errors' => $validacion->errors()
            ],422);
        }

        try{
            DB::beginTransaction();
            $hash = base64_decode($request->hash);
            $email =   base64_decode($request->email);

            $user = Usuario::where('email', $email)
            ->whereHas('_recovery', function($q) use ($hash){
                $q->where('token',$hash);
            })
            ->with('_recovery')
            ->first();

            $fecha = date('Y-m-d H:i:s');

            //verificaciones
            if(empty($user)) throw new \Exception('Un error ha ocurrido, porfavor vuelva a intentarlo mas tarde.');
            if($user->_recovery->expires_at->lt($fecha)) throw new \Exception('La recuperacion ha expirado, porfavor vuelva a solicitar su cambio de clave'); 
            //modificacion de la clave
            $user->password = Hash::make($request->password);
            $user->update();

            $user->_recovery->delete();

            DB::commit();
            return response([
                'success' => 'Su clave ha sido cambiada con exito.'
            ],200);

        }catch(\Exception $e){

            DB::rollback();
            return response([
                'error'=>$e->getMessage()
            ],500);
        }

        if($validacion->fails()){
            return response([
                'errors' => $validacion->errors()
            ],422);
        }        
    }

    private function enviarNotificacion ($user ,$token  = null)
    {           
        if($user){
            if($token){
                $correo = base64_encode($user->email);
                $token = base64_encode($token->token);
                $url =  config('app.env') == 'local' ? 'http://localhost:8080/recuperar' : 'http://isnotjs.com/recuperar';
                $url = $url.'/'.$correo.'/'.$token; 
            }

            $action = [
                'texto' => 'Ingresar a la web',
                'url' => $url,
            ];
            $notificacion = array(
                'subject' => 'Recuperacion de contraseña',
                'title' => 'Recuperacion de contraseña',
                'message' => 'Ingrese al siguiente enlaze para recuperar su contraseña',
                'action'=>$action
            );
            $user->notify(new NotificationUser($notificacion));
            
            return true;
        }

        return false;
    }
}
