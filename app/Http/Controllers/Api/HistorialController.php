<?php
namespace App\Http\Controllers\Api;

use DB;
use Auth;
use App\Models\Comun\Pais;
use Illuminate\Http\Request;
use App\Models\Cliente\Banco;
use App\Models\Comun\Subtipo;
use App\Models\Cliente\Cuenta;
use App\Http\Controllers\Controller;
use App\Models\Cliente\Transferencia;
use App\Models\Cliente\TransferenciaDetalle;

class HistorialController extends Controller
{
    public function index(Request $request){

        try {
            $transferencias = TransferenciaDetalle::buscar($request)
            ->whereIn('id_estado',[5,6,7,9,10])
            ->with('_estado', '_banco','_moneda_envio','_moneda_recibo','_imagen')
            ->with('_tipo_cuenta', '_moneda_envio', '_moneda_recibo')
            ->whereHas('_transferencia',function($q){
                $q->where('usuario_id', Auth::user()->id);
            })
            ->orderBy('id','DESC')
            ->paginate();

            $bancos = Banco::with('_tipos')->get();
            $cuentas = Cuenta::where('usuario_id',Auth::user()->id)
            ->with('_banco' , '_tipo_cuenta')
            ->get();

            return response([
                'cuentas' => $cuentas,
                'transferencias' => $transferencias,
                'bancos' => $bancos,
            ]);

        } catch (\Exception $e) {

            return response([
                'error' => $e->getMessage(),
            ],500);
        }
    }
}
