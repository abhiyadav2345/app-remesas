<?php

namespace App\Http\Controllers\Api;

use DB;
use Auth;
use App\Models\Comun\Pais;
use Illuminate\Http\Request;
use App\Models\Comun\Tasa;
use App\Models\Cliente\Cuenta;
use App\Http\Controllers\Controller;

class TasasController extends Controller
{
    public function index(Request $request){

        try {

            $tasas = Pais::with(['_tasa_origen' => function($q){
                $q->where("estado",1)
                ->with('_pais_destino');
            }])
            ->whereHas("_tasa_origen", function($q){
                $q->where("estado",1);
            })
            ->get();

            return response([
                'tasas' => $tasas,
            ]);

        } catch (\Exception $e) {
            return response([
                'error' => $e->getMessage(),
            ],500);
        }
    }

   
}
