<?php

namespace App\Http\Controllers\Api;

use DB;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cliente\Cuenta;
use App\Models\Comun\Tasa;
use App\Models\Comun\Subtipo;
use App\Interfaces\CrudSimple;
use App\Models\Comun\Pais;
use App\Models\Cliente\Transferencia;
use App\Models\Cliente\TransferenciaDetalle;
use App\Models\Cliente\Banco;

class TransferenciaController extends Controller implements CrudSimple
{
    public function index(Request $request)
    {

        try {
            $tasas = Pais::whereHas("_tasa_origen", function($q){
                $q->where("estado",1);
            })
            ->with(['_tasa_origen' => function($q){
                $q->where("estado",1)
                ->with('_pais_destino');
            }])
            ->get();

            $cuentas = Cuenta::where('usuario_id',Auth::user()->id)
            ->with('_banco' , '_tipo_cuenta')
            ->get();
            $bancos = Banco::with('_tipos')->get();
            $tipos_operaciones = Subtipo::where('tipo_id',2)->get();

            return response([
                'tasas' => $tasas,
                'cuentas' => $cuentas,
                'tipos_operaciones' => $tipos_operaciones,
                'bancos' => $bancos
            ]);

        } catch (\Exception $e) {
            return response([
                'error' => $e->getMessage(),
            ],500);
        }
    }

    public function guardar (Request $request)
    {
        try {
            $validar = $this->validar($request->all(), [
                'monto' => 'required|numeric',
                'id_tipo_operacion' => 'required|integer|exists:com_subtipos,id',
                'file' => 'required',
                'origen' => 'required|integer|exists:com_paises,id',
                'destino' => 'required|integer|exists:com_paises,id',
            ]);

            if(!$validar->status){
                return response([
                    'error' => $validar->errors
                ],500);
            }

            DB::beginTransaction();

            $origen = Pais::with(['_tasa_origen' => function($q) use ($request){
                $q->where("estado", 1)
                ->where('pais_destino_id', $request->destino);
            }])
            ->where('id', $request->origen)
            ->first();

            $destino = Pais::find($request->destino);
            $tasa = $origen->_tasa_origen[0];

            $transferencia = new Transferencia;
            $transferencia->moneda_id = $origen->moneda_id;
            $transferencia->monto = $request->monto;
            $transferencia->usuario_id = Auth::user()->id;
            $transferencia->id_tipo_operacion = $request->id_tipo_operacion;
            $transferencia->tasa = $tasa->tasa;
            $transferencia->pais_origen_id = $origen->id;
            $transferencia->pais_destino_id = $destino->id;
            $transferencia->save();

            //Imagen
            $imagen_save = $this->guardarFile($request->file, $transferencia);
            if(!$imagen_save->status){
                throw new \Exception($imagen_save->errors);
            }

            foreach ($request->detalle as $key => $value) {
                $detalle = new TransferenciaDetalle;

                $detalle->transferencia_id = $transferencia->id;
                $detalle->razon_social = $value['razon_social'];
                $detalle->cedula = $value['cedula'];
                $detalle->banco_id = $value['banco_id'];
                $detalle->monto_envio = number_format((float)$value['monto_envio'], 2, ',', '.');
                $detalle->moneda_envio_id = $origen->moneda_id;
                $detalle->id_tipo_cuenta = $value['id_tipo_cuenta'];
                $detalle->cuenta_id = !empty($value['cuenta_id']) ? $value['cuenta_id'] : null;
                $detalle->numero_cuenta = $value['numero_cuenta'];
                $detalle->monto_recibido = number_format(((float)$tasa->tasa * (float)$value['monto_envio']),2,',','.'); //este formato conviene pa JS
                $detalle->moneda_recibo_id = $destino->moneda_id;
                $detalle->id_estado = 5;
                $detalle->save();
            }

            DB::commit();

            return response([
                'success' => 'Operacion realiza con exito.'
            ]);

        } catch (\Exception $e) {
            DB::rollBack();
            return response([
                'error' => $e->getMessage()
            ],500);
        }

    }

    public function actualizar(Request $request)
    {
        try {
            
            $validar = $this->validar($request->all(), [
                'id' => 'required|exists:clt_transferencias_detalles,id',
                'cuenta_id' => 'nullable|exists:clt_cuentas,id',
                'banco_id' => 'nullable|exists:clt_bancos,id',
                'id_tipo_cuenta' => 'nullable|exists:com_subtipos,id',
                'razon_social' => 'required|string',
                'cedula' => 'required|string',
                'numero_cuenta' => 'required|string'
            ]);

            if(!$validar->status){
                return response([
                    'error' => $validar->errors
                ],500);
            }

            DB::beginTransaction();

            $transferencia = TransferenciaDetalle::find($request->id);
            $transferencia->fill($request->except('id'));
            $transferencia->id_estado = 5;
            $transferencia->update();
            $transferencia->load('_estado','_banco','_moneda_envio','_moneda_recibo','_tipo_cuenta','_moneda_envio','_moneda_recibo');

            DB::commit();
            return response([
                'success' => 'Operacion realiza con exito.',
                'transferencia' => $transferencia
            ]);

        } catch (\Exception $e) {
            DB::rollBack();
            return response([
                'error' => $e->getMessage()
            ],500);
        }

    }

    public function eliminar(Request $request)
    {
        try {
            DB::beginTransaction();

            DB::commit();
            return response([
                'success' => 'Operacion realiza con exito.'
            ]);

        } catch (\Exception $e) {
            DB::rollBack();
            return response([
                'error' => $e->getMessage()
            ]);
        }

    }
}
