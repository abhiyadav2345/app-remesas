<?php

namespace App\Http\Controllers\Comun;

use DB;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Interfaces\CrudSimple;

use App\Models\Comun\Tipo;

class TipoController extends Controller implements CrudSimple
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
        try {
            $tipos = Tipo::buscar($request)
            ->withCount('_subtipos')
            ->paginate();

            return response($tipos, 200);
            
        } catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error'=>$mensaje], 500);
        }
    }

    public function guardar(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string',
            'apodo' => 'required|string',
            'descripcion' => 'nullable|string'
        ]);

        try {
            DB::beginTransaction();

            $tipo = new Tipo;
            $tipo->fill($request->all());
            $tipo->save();

            DB::commit();

            $tipo = $tipo->loadCount('_subtipos');

            $mensaje = ['Tipo guardado exitosamente'];
            return response(compact('tipo', 'mensaje'), 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function actualizar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:com_tipos,id',
            'nombre' => 'required|string',
            'apodo' => 'required|string',
            'descripcion' => 'nullable|string'
        ]);

        try {
            DB::beginTransaction();

            $tipo = Tipo::find($request->id);
            $tipo->fill($request->all());
            $tipo->save();

            DB::commit();

            $tipo = $tipo->loadCount('_subtipos');

            $mensaje = ['Tipo actualizado exitosamente'];
            return response(compact('tipo', 'mensaje'), 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function eliminar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:com_tipos,id'
        ]);

        try {
            DB::beginTransaction();

            $tipo = Tipo::findOrFail($request->id);
            $tipo->delete();

            DB::commit();

            $mensaje = ['Tipo eliminado exitosamente'];
            return response(['mensaje' => $mensaje], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }
}
