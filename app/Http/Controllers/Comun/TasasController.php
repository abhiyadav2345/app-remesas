<?php

namespace App\Http\Controllers\Comun;

use DB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Comun\Tasa;
use App\Models\Comun\Pais;
use App\Models\Comun\TasaTraza;

class TasasController extends Controller
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
        try {
            $tasas = Tasa::buscar($request)
            ->with('_pais_origen','_pais_destino')
            ->paginate();

            $paises = Pais::get();

            return response([
                'tasas' => $tasas,
                'paises' => $paises,
            ],200);

        } catch (\Exception $e) {

            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error'=>$mensaje], 500);
        }
    }

    public function crearEdit(Request $request)
    {
        $this->validate($request, [
            'id' => 'nullable|integer',
            'pais_origen_id' => 'required_without:id|integer',
            'pais_destino_id' => 'required_without:id|integer',
            'tasa' => 'required|numeric',
            'monto_min' => 'required|numeric',
        ]);

        try {

            if(!$request->id){

                $exist = Tasa::where('pais_origen_id', $request->pais_origen_id)
                ->where('pais_destino_id',$request->pais_destino_id)
                ->first();

                if($exist) throw new \Exception("Ya existe una tasa para estos paises, actulize los datos");
            }

            DB::beginTransaction();

            if(!$request->id){
                $tasa = new Tasa;
                $tasa->fill($request->all());
            }else{
                $tasa = Tasa::find($request->id);
                $tasa->tasa = $request->tasa;
                $tasa->monto_min = $request->monto_min;
            }

            $tasa->save();

            $tasa->load('_pais_origen', '_pais_destino');
            $traza = TasaTraza::create($tasa->toArray());

            DB::commit();

            return response([
                'success' => 'Tipo guardado exitosamente',
                'tasa'=>$tasa,
            ], 200);
        }
        catch(\Exception $e) {

            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function estado(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:clt_tasas,id',
        ]);

        try {
            DB::beginTransaction();

            $tasa = Tasa::find($request->id);
            $tasa->estado = $tasa->estado == 1 ? 0 : 1;
            $tasa->save();

            $tasa->load('_pais_origen', '_pais_destino');

            DB::commit();
            return response([
                'success' => 'Operacion realizada exitosamente',
                'tasa' => $tasa,
            ],200);
        }
        catch(\Exception $e) {

            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }
}
