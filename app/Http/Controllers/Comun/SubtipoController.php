<?php

namespace App\Http\Controllers\Comun;

use DB;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Interfaces\CrudSimple;

use App\Models\Comun\Subtipo;
use App\Models\Comun\Tipo;

class SubtipoController extends Controller implements CrudSimple
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
        try {
            $tipo = Tipo::findOrFail($request->idTipo);
            $subtipos = Subtipo::buscar($request)->paginate();

            return response(compact('subtipos', 'tipo', 200));
            
        } catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function guardar(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string',
            'tipo_id' => 'required|integer|exists:com_tipos,id',
            'descripcion' => 'nullable|string'
        ]);

        try {
            DB::beginTransaction();

            $subtipo = new Subtipo;
            $subtipo->fill($request->all());
            $subtipo->save();

            DB::commit();

            $mensaje = ['Subtipo guardado exitosamente'];
            return response(compact('subtipo', 'mensaje'), 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function actualizar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:com_subtipos,id',
            'nombre' => 'required|string',
            'tipo_id' => 'required|integer|exists:com_tipos,id',
            'descripcion' => 'nullable|string'
        ]);

        try {
            DB::beginTransaction();

            $subtipo = Subtipo::find($request->id);
            $subtipo->fill($request->all());
            $subtipo->save();

            DB::commit();

            $mensaje = ['Subtipo actualizado exitosamente'];
            return response(compact('subtipo', 'mensaje'), 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function eliminar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:com_subtipos,id'
        ]);

        try {
            DB::beginTransaction();

            $subtipo = Subtipo::findOrFail($request->id);
            $subtipo->delete();

            DB::commit();

            $mensaje = ['Subtipo eliminado exitosamente'];
            return response(['mensaje' => $mensaje], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }
}
