<?php

namespace App\Http\Controllers\Sistema;

use DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;
use App\Interfaces\CrudComplete;

use App\Models\Comun\Pais;
use App\Models\Sistema\Administrador;
use App\Models\Sistema\Seccion;

class AdministradorController extends Controller implements CrudComplete
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
    	try {
            $administradores = Administrador::buscar($request)
            ->with('_pais')
            ->paginate(10);

            return response(['administradores' => $administradores], 200);

        } 
        catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function crear(Request $request)
    {
    	try {
            $paises = Pais::get();

            return response(['paises' => $paises], 200);

        } 
        catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

	public function guardar(Request $request)
	{
		$this->validate($request, [
            'nombre' => 'required|string',
            'apellidos' => 'required|string',
            'cedula' => 'required|string',
            'telefono' => 'nullable|string',
            'direccion' => 'sometimes|string',
            'email' => 'required|email:rfc,dns|unique:adm_usuarios',
            'pais_id' => 'required|exists:com_paises,id',
            'clave' => 'required|confirmed|min:8',
        ]);

        try {
            DB::beginTransaction();
            
            $admin = new Administrador;
            $admin->fill($request->except('clave'));
            $admin->password = Hash::make($request->clave);
            $admin->save();

            DB::commit();

            $mensaje = ['Usuario del sistema administrador guardado exitosamente'];
            return response($mensaje, 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
	}

	public function editar(Request $request)
	{
		$this->validate($request, [
            'id' => 'required|exists:adm_usuarios,id'
        ]);

        try {
            $admin = Administrador::with('_pais')->find($request->id);
            $paises = Pais::get();

            return response(compact('admin', 'paises'), 200);

        } 
        catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
	}

	public function actualizar(Request $request)
	{

	}

	public function desactivar(Request $request)
	{
        $this->validate($request, [
            'id' => 'required|integer|exists:adm_usuarios,id'
        ]);

        try {
            DB::beginTransaction();

            $admin = Administrador::findOrFail($request->id);
            $admin->activo = 0;
            $admin->save();

            DB::commit();

            $mensaje = ['Usuario del sistema administrador desactivado exitosamente'];
            return response([
                'admin' => $admin,
                'success' => $mensaje
            ], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
	}

	public function reactivar(Request $request)
	{
        $this->validate($request, [
            'id' => 'required|integer|exists:adm_usuarios,id'
        ]);

        try {
            DB::beginTransaction();

            $admin = Administrador::findOrFail($request->id);
            $admin->activo = 1;
            $admin->save();

            DB::commit();

            $mensaje = ['Usuario del sistema administrador reactivado exitosamente'];
            return response([
                'admin' => $admin,
                'success' => $mensaje
            ], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
	}

	public function eliminar(Request $request) 
	{

	}

    public function editar_clave(Request $request)
    {
        $this->validate($request, [
            'id' =>'required|integer|exists:adm_usuarios,id',
            'password' => 'required|string|confirmed|min:8',
        ]);

        try {
            DB::beginTransaction();

            $admin = Administrador::where('id', $request->id)->first();
            $admin->password = Hash::make($request->password);
            $admin->save();

            DB::commit();

            $mensaje = ['Clave de usuario modificada exitosamente'];

            return response([
                'success' => $mensaje,
                'admin' => $admin
            ],200);

        }
        catch(\Exception $e){
            DB::rollback();

            return response(['error'=> $e->getMessage()],500);
        }
    }

    public function permisos_listado(Request $request)
    {
        try {
            $admin = Administrador::find($request->id);

            $secciones = Seccion::with('_menus._acciones')
            ->orderBy('orden')
            ->get();

            $permisos = DB::table('adm_usuarios_acciones')
            ->where('usuario_id', $request->id)
            ->pluck('accion_id')
            ->toArray();

            foreach($secciones as $seccion) {
                foreach($seccion->_menus as $menu) {
                    foreach($menu->_acciones as $accion){
                        if(in_array($accion->id, $permisos)) {
                            $accion['check'] = true;
                        }
                        else {
                            $accion['check'] = false;
                        }
                    }
                }
            }

            return response(compact('secciones', 'admin'), 200);

        } catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function permisos_guardar(Request $request)
    {
        

        try {
            DB::beginTransaction();

            $permisos = $request->permisos;

            $usuarios_acciones = DB::table('adm_usuarios_acciones')
            ->where('usuario_id', $request->id)
            ->get();

            foreach ($usuarios_acciones as $key => $usuario_accion) {
                if(!in_array($usuario_accion->accion_id, $permisos)) {
                    $datos = array(
                        'accion_id' => $usuario_accion->accion_id,
                        'perfil_id' => $usuario_accion->perfil_id
                    );
                    DB::table('adm_usuarios_acciones')->where($datos)->delete();
                }
                else {
                    $key = array_search($usuario_accion->accion_id, $permisos);
                    array_splice($permisos, $key, 1);
                }
            }

            foreach($permisos as $permiso) {
                DB::table('adm_usuarios_acciones')->insert([
                    'usuario_id' => $request->id,
                    'accion_id' => $permiso
                ]);
            }

            DB::commit();

            $mensaje = ['Permisos modificados exitosamente'];
            return response(['success' => $mensaje], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }
}
