<?php

namespace App\Http\Controllers\Sistema;

use DB;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Interfaces\CrudComplete;

use App\Models\Sistema\Accion;
use App\Models\Sistema\Menu;

class AccionController extends Controller implements CrudComplete
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
        try {
            $acciones = Accion::buscar($request)
            ->orderBy('orden')
            ->paginate();

            $menu = Menu::find($request->menu_id);

            return response(compact('acciones', 'menu'), 200);

        } catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function crear(Request $request) 
    {
        try {
            $menu = Menu::find($request->menu_id);
            return response(['menu' => $menu], 200);

        } catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
        
    }

    public function editar(Request $request) 
    {
        try {
            $accion = Accion::find($request->id);
            $menu = Menu::find($request->menu_id);
            return response(compact('accion', 'menu'), 200);
            
        } catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
        
    }

    public function guardar(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string',
            'accion' => 'required|string',
            'menu_id' => 'required|numeric|exists:adm_menus,id',
            'orden' => 'required|numeric'
        ]);

        try {
            DB::beginTransaction();

            $accion = new Accion;
            $accion->fill($request->all());
            $accion->save();

            DB::commit();

            $mensaje = ['Acción guardada exitosamente'];
            return response(['success' => $mensaje], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function actualizar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:adm_acciones',
            'nombre' => 'required|string',
            'accion' => 'required|string',
            'menu_id' => 'required|numeric|exists:adm_menus,id',
            'orden' => 'required|numeric'
        ]);

        try {
            DB::beginTransaction();

            $accion = Accion::findOrFail($request->id);
            $accion->fill($request->all());
            $accion->save();

            DB::commit();

            $mensaje = ['Acción actualizada exitosamente'];
            return response(['success' => $mensaje], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }


    public function eliminar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:adm_acciones'
        ]);

        try {
            DB::beginTransaction();
            $accion = Accion::findOrFail($request->id);
            $accion->delete();
            DB::commit();

            $mensaje = ['Acción eliminada exitosamente'];
            return response(['success' => $mensaje], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }
}
