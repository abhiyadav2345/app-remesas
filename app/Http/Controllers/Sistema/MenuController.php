<?php

namespace App\Http\Controllers\Sistema;

use DB;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Interfaces\CrudComplete;

use App\Models\Sistema\Menu;
use App\Models\Sistema\Seccion;
use App\Helpers\FontAwesome;

class MenuController extends Controller implements CrudComplete
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
        try {
            $menus = Menu::buscar($request)
            ->orderBy('orden')
            ->paginate();

            $seccion = Seccion::find($request->seccion_id);

            return response(compact('menus', 'seccion'), 200);

        } catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    	
    }

    public function crear(Request $request) 
    {
        try {
            $seccion = Seccion::find($request->seccion_id);
            $iconos = FontAwesome::listIcons();
            return response(compact('seccion', 'iconos'), 200);

        } catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
        
    }

    public function editar(Request $request) 
    {
        try {
            $menu = Menu::find($request->id);
            $seccion = Seccion::find($request->seccion_id);
            $iconos = FontAwesome::listIcons();
            return response(compact('menu', 'seccion', 'iconos'), 200);
            
        } catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
        
    }

    public function guardar(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string',
            'ruta' => 'required|string',
            'seccion_id' => 'required|numeric|exists:adm_secciones,id',
            'icono' => 'required|string',
            'orden' => 'required|numeric'
        ]);

        try {
            DB::beginTransaction();

            $menu = new Menu;
            $menu->fill($request->all());
            $menu->save();

            DB::commit();

            $mensaje = ['Menú guardado exitosamente'];
            return response(['success' => $mensaje], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function actualizar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:adm_menus',
            'nombre' => 'required|string',
            'ruta' => 'required|string',
            'seccion_id' => 'required|numeric|exists:adm_secciones,id',
            'icono' => 'required|string',
            'orden' => 'required|numeric'
        ]);

        try {
            DB::beginTransaction();

            $menu = Menu::findOrFail($request->id);
            $menu->fill($request->all());
            $menu->save();

            DB::commit();

            $mensaje = ['Menú actualizado exitosamente'];
            return response(['success' => $mensaje], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function eliminar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:adm_menus'
        ]);

        try {
            DB::beginTransaction();
            $menu = Menu::findOrFail($request->id);
            $menu->delete();
            DB::commit();

            $mensaje = ['Menú eliminada exitosamente'];
            return response(['success' => $mensaje], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }
}
