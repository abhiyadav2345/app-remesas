<?php

namespace App\Http\Controllers;

use Validator;

use Storage;
use App\Models\Comun\Imagen; 
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function validar($datos, $validaciones)
    {
        $errors = [];
        $status = true;

        $validator = Validator::make($datos,$validaciones);

        if($validator->fails()){
            $errors = $validator->errors();
            $status = false;
        }

        return (object)[
            'status' => $status,
            'errors' => $errors,
        ];
    }

    public function guardarFile ($file,$model,$nombre = null, $ruta = '\\', $disk = 'public')
    {   
        $response = (object)[
            'status' => true,
            'errors' => '',
            'url' => ''
        ];

        try {
            $originalName = $file->getClientOriginalName();
            $filename = $nombre ? $nombre : time().'.'.$file->getClientOriginalExtension(); 
            $model_name = get_class($model);
            $key = $model->getKeyName();
            $model_id = $model->$key;
            $base = config('filesystems.server.root').'/';
            $rutaServer = $base.$disk.$ruta.$filename;

            Imagen::create([
                'nombre' => $filename, 
                'nombre_original' => $originalName, 
                'ruta' => $rutaServer, 
                'imageable_id' => $model_id, 
                'imageable_type' => $model_name
            ]);
            
            Storage::disk($disk)->putFileAs($ruta, $file, $filename);

            $response->url = config('filesystems.server.url').'/'.$disk.$ruta.$filename;

            return $response;

        } catch (\Exception $e) {
            $response->status = false;
            $response->errors = $e->getMessage();

            return $response;
        }
    }

    public function hide_partial_number ($number, $char_show = 4) 
    {
        $hide = str_repeat('*', strlen((string) $number) - $char_show);
        $show = substr((string) $number, -$char_show);
        $result = $hide . $show;
        return $result;
    }
           
}
