<?php

namespace App\Http\Controllers\Cliente;

use DB;
use Mail;
use Storage;

use App\Mail\Notificacion;
use App\Notifications\NotificationUser;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Comun\Subtipo;
use App\Models\Cliente\Rechazo;
use App\Models\Cliente\Transferencia;
use App\Models\Cliente\TransferenciaDetalle;

class TransferenciaDetalleController extends Controller
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
    	$detalle = TransferenciaDetalle::buscar($request)
    	->with('_banco', '_moneda_envio', '_estado')
    	->with(['_transferencia' => function($q) {
    		$q->with('_pais_origen', '_pais_destino');
    	}])
        ->whereIn('id_estado', [5, 6])
    	->paginate();

    	$estados = Subtipo::selectEspecificsOptions('estados', ['creado', 'verificando'])
        ->get();

    	return response([
    		'detalle' => $detalle,
    		'estados' => $estados
    	], 200);
    }

    public function detalles(Request $request)
    {
    	$this->validate($request, [
    		'id' => 'required|integer|exists:clt_transferencias_detalles'
    	]);

    	$detalle = TransferenciaDetalle::with('_banco', '_estado')
    	->with('_tipo_cuenta', '_moneda_envio', '_moneda_recibo', '_imagen')
    	->with(['_transferencia' => function($q) {
    		$q->with('_pais_origen', '_pais_destino', '_usuario', '_tipo_operacion');
    	}])
    	->find($request->id);

    	return response ([
    		'detalle' => $detalle
    	], 200);
    }

    public function aceptar (Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:clt_transferencias_detalles'
        ]);

        try {
            DB::beginTransaction();

            $detalle = $this->cambiarEstado($request->id, 'verificando', true);

            if (config('app.env') == 'local') {

                $this->enviarNotificacion('aceptado', $detalle);

            }

            DB::commit();

            $success = 'Gestión en proceso de verificación y notificada al cliente exitosamente';

            return response([
                'detalle' => $detalle,
                'success' => $success
            ], 200);

        }
        catch(\Exception $e) {
            DB::rollback();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function finalizar (Request $request)
    {
    	$this->validate($request, [
    		'id' => 'required|integer|exists:clt_transferencias_detalles',
            'file' => 'required|file|mimes:jpeg,jpg,png,gif'
    	]);

    	try {
    		DB::beginTransaction();

    		$detalle = $this->cambiarEstado($request->id, 'transferido');

            $imagen = $this->guardarFile($request->file, $detalle, null, '/comprobantes/');

            if(!$imagen->status){
                throw new \Exception($imagen->errors);
            }

    		if (config('app.env') == 'local') {

                $this->enviarNotificacion(
                    'verificado', $detalle, ['setOrigin', 'setReceiver'], null, $imagen
                );

            }

            DB::commit();

            $success = 'Gestión finalizada y notificada al cliente exitosamente';

            return response([
    			'detalle' => $detalle,
    			'success' => $success
    		], 200);

    	}
    	catch(\Exception $e) {
    		DB::rollback();
    		$mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
    	}
    } 

    public function rechazar (Request $request)
    {
    	$this->validate($request, [
    		'id' => 'required|integer|exists:clt_transferencias_detalles',
    		'motivo' => 'required|string'
    	]);

    	try {
            DB::beginTransaction();

    		$detalle = $this->cambiarEstado($request->id, 'rechazado');

    		$rechazo = new Rechazo;
    		$rechazo->motivo = $request->motivo;
    		$rechazo->transferencia_detalle_id = $request->id;
    		$rechazo->save();

            if (config('app.env') == 'local') {

                $this->enviarNotificacion(
                    'rechazado', $detalle, ['setDetailHeader', 'setReceiver'], $rechazo->motivo
                );

            }

            DB::commit();

			$success = 'La transferencia ha sido rechazada y notificada al cliente exitosamente';

    		return response([
    			'detalle' => $detalle,
    			'success' => $success
    		], 200);

    	}
    	catch(\Exception $e) {
            DB::rollback();
    		$mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
    	}
    }

    public function contactar_soporte (Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:clt_transferencias_detalles',
        ]);

        try {
            DB::beginTransaction();

            $detalle = $this->cambiarEstado($request->id, 'contactar a soporte');

            if (config('app.env') == 'local') {

                $this->enviarNotificacion('contacto_soporte', $detalle);
            }
            
            DB::commit();

            $success = 'Cambio de estado de transferencia y notificación al cliente exitosa';

            return response([
                'detalle' => $detalle,
                'success' => $success
            ], 200);

        }
        catch(\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }

    }

    private function cambiarEstado ($id, $estado, $withRelashionship = false)
    {
		$estado = Subtipo::selectEspecificsOptions('estados', $estado)->first();

		if(!$estado) {
			throw new \Exception(
                "El estado '$request->estado' no existe en la tabla de subtipos"
            );
		}

		$detalle = TransferenciaDetalle::find($id);
		$detalle->id_estado = $estado->id;
		$detalle->save();

        if($withRelashionship) {
            $detalle->load('_banco', '_moneda_envio', '_moneda_recibo', '_estado')
            ->load(['_transferencia' => function($q) {
                $q->with('_pais_origen', '_pais_destino');
            }]);
        }

		return $detalle;
    }

    private function enviarNotificacion ($typeMessage, $detalle, $include=[], $motivo=null, $img=null)
    {
        $subject = [
            'aceptado' => 'Verificando transferencia',
            'verificado' => 'Transferencia efectuada exitosamente',
            'rechazado' => 'Transferencia rechazada',
            'contacto_soporte' => 'Transferencia rechazada y finalizada'
        ];

        $title = [
            'aceptado' => 'Transferencia en proceso de verificación',
            'verificado' => 'Proceso terminado',
            'rechazado' => 'Transferencia rechazada',
            'contacto_soporte' => 'Por favor contacte con soporte'
        ];

        $message = [
            'aceptado' => "La transferencia con ID #$detalle->id
                se encuentra en proceso de verificación",

            'verificado' => "La transferencia con ID #$detalle->id
                se ha realizado de forma exitosa: <br><br>",

            'rechazado' => "La transferencia con ID #$detalle->id
                ha sido rechazada, el motivo es '$motivo'",

            'contacto_soporte' => "La gestión de la transferencia 
                con ID #$detalle->id ha sido rechazada y finalizada, 
                por favor contacte con soporte si cree que existe un error."
        ];

        // Construyendo el contenido del mensaje
        $msg = $message[$typeMessage];

        if(in_array('setDetailHeader', $include)) {
            $msg .= "<br><br>";
            $msg .= "Los detalles de la misma son los siguientes: <br>";
        }

        if(in_array('setOrigin', $include)) {
            $msg .= "Origen: ". $detalle->_transferencia->_pais_origen->nombre ."<br>";
            $msg .= "Monto transferido: $detalle->monto_envio ";
            $msg .= $detalle->_moneda_envio->diminutivo ."<br>";
        }

        if(in_array('setReceiver', $include)) {
            $msg .= "Destinatario: $detalle->razon_social <br>";
            $msg .= "Cédula: $detalle->cedula <br>";
            $msg .= "Destino: ". $detalle->_transferencia->_pais_destino->nombre ."<br>";
            $msg .= "Monto a recibir: $detalle->monto_recibido ";
            $msg .= $detalle->_moneda_recibo->diminutivo ."<br>";
            $msg .= "Banco: ".$detalle->_banco->razon_social ."<br>";
            $msg .= "Número de cuenta: ". $this->hide_partial_number($detalle->numero_cuenta);
        }
                
        $notificacion = array(
            'subject' => $subject[$typeMessage],
            'title' => $title[$typeMessage],
            'message' => $msg
        );

        if(isset($img)) {
            $notificacion['image'] = $img->url;
        }

        $detalle->_transferencia->_usuario->notify(new NotificationUser($notificacion));
    }

}
