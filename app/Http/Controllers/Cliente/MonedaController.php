<?php

namespace App\Http\Controllers\Cliente;

use DB;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Interfaces\CrudSimple;

use App\Models\Cliente\Moneda;

class MonedaController extends Controller implements CrudSimple
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
        try {
            $monedas = Moneda::buscar($request)->paginate();

            return response([
            	'monedas' => $monedas
            ], 200);
        } 
        catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function guardar(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string|max:80',
            'diminutivo' => 'required|string|max:10'
        ]);

        try {
            DB::beginTransaction();

            $moneda = new Moneda;
            $moneda->fill($request->all());
            $moneda->save();

            DB::commit();

            $mensaje = ['Moneda guardada exitosamente'];
            return response(compact('moneda', 'mensaje'), 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function actualizar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:clt_monedas,id',
            'nombre' => 'required|string|max:80',
            'diminutivo' => 'required|string|max:10'
        ]);

        try {
            DB::beginTransaction();

            $moneda = Moneda::find($request->id);
            $moneda->fill($request->all());
            $moneda->save();

            DB::commit();

            $mensaje = ['Moneda actualizada exitosamente'];
            return response(compact('moneda', 'mensaje'), 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function eliminar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:clt_monedas,id'
        ]);

        try {
            DB::beginTransaction();

            $moneda = Moneda::findOrFail($request->id);
            $moneda->delete();

            DB::commit();

            $mensaje = ['Moneda eliminada exitosamente'];
            return response(['mensaje' => $mensaje], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }
}
