<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
	const UPDATED_AT = null;
    protected $table = 'adm_configuraciones';

    protected $fillable = [
    	'envios'
    ];
}
