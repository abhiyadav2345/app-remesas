<?php

namespace App\Models\Sistema;

use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Administrador extends Authenticatable
{
    use Notifiable, SoftDeletes;
    protected $table = 'adm_usuarios';

    protected $fillable = [
        'nombre', 'apellidos', 'cedula', 'email', 'pais_id', 'password', 'telefono',
        'activo', 'remember_token'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'nombre_completo'
    ];

    public function _pais ()
    {
        return $this->belongsTo('App\Models\Comun\Pais', 'pais_id');
    }

    public function _acciones () // NU
    {
        return $this->hasManyThrough(
            Accion::class,
            AdministradorAccion::class,
            'usuario_id', // Foreign key de pivote a Usuarios
            'id', // Primary key Acciones
            'id', // Primary key Usuarios
            'accion_id' // Foreign key de pivote a Acciones
        );
    }

    public function _secciones ()
    {
        return DB::table('adm_usuarios_acciones')
        ->join('adm_acciones', 'adm_acciones.id', '=', 'accion_id')
        ->join('adm_menus', 'adm_menus.id', '=', 'adm_acciones.menu_id')
        ->join('adm_secciones', 'adm_secciones.id', '=', 'seccion_id')
        ->select('adm_secciones.id', 'adm_secciones.nombre', 'adm_secciones.ruta', 'adm_secciones.icono')
        ->distinct('adm_secciones.id')
        ->where('usuario_id', $this->id)
        ->where('adm_acciones.nombre', 'ingresar')
        ->get();
    }

    public function _menus($seccion_id)
    {
        return DB::table('adm_usuarios_acciones')
        ->join('adm_acciones', 'adm_acciones.id', '=', 'accion_id')
        ->join('adm_menus', 'adm_menus.id', '=', 'adm_acciones.menu_id')
        ->select('adm_menus.nombre', 'adm_menus.seccion_id', 'adm_menus.ruta', 'adm_menus.icono')
        ->where('usuario_id', $this->id)
        ->where('adm_acciones.nombre', 'ingresar')
        ->where('adm_menus.seccion_id', $seccion_id)
        ->orderBy('adm_menus.orden')
        ->get();
    }

    public function getNombreCompletoAttribute()
    {
        return $this->nombre.' '.$this->apellidos;
    }

    public function scopeBuscar($query, $request)
    {
        if($request->id) {
            $query->where('id', $request->id);
        }
        if($request->nombre) {
            $query->where('nombre', 'LIKE', "%$request->nombre%");
        }
        if($request->email) {
            $query->where('email', 'LIKE', "%$request->email%");
        }
        if(isset($request->estado)) {
            $query->where('activo', $request->estado);
        }

        return $query;
    }
}
