<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Moneda extends Model
{
	use SoftDeletes;

    protected $table = 'clt_monedas';

    protected $fillable = [
    	'nombre', 'diminutivo'
    ];

    public function scopeBuscar($query, $request)
    {
    	if($request->id) {
    		$query->where('id', $request->id);
    	}

    	if($request->nombre) {
    		$query->where('nombre', 'LIKE', '%'.$request->nombre.'%');
    	}

    	if($request->diminutivo) {
    		$query->where('diminutivo', 'LIkE', '%'.$request->diminuto.'%');
    	}

    	return $query;
    }
}
