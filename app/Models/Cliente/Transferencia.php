<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transferencia extends Model
{
    use SoftDeletes;

    protected $table = 'clt_transferencias';

    protected $fillable = [
        'id', 'usuario_id', 'monto', 'moneda_id', 'id_tipo_operacion', 'tasa',
        'pais_origen_id', 'pais_destino_id'
    ];

    protected $dates =['created_at', 'updated_at', 'deleted_at'];

    public function _estado ()
    {
    	return $this->belongsTo('App\Models\Comun\Subtipo', 'id_estado');
    }

    public function _usuario ()
    {
    	return $this->belongsTo('App\Models\Cliente\Usuario', 'usuario_id');
    }

    public function _tipo_operacion ()
    {
    	return $this->belongsTo('App\Models\Comun\Subtipo', 'id_tipo_operacion');
    }

    public function _imagenes ()
    {
    	return $this->morphMany('App\Models\Comun\Imagen', 'imageable');
    }

    public function _pais_origen ()
    {
        return $this->belongsTo('App\Models\Comun\Pais', 'pais_origen_id');
    }

    public function _pais_destino ()
    {
        return $this->belongsTo('App\Models\Comun\Pais', 'pais_destino_id');
    }
    
}
