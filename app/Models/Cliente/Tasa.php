<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;

class Tasa extends Model
{
    const UPDATED_AT = null;
    protected $table = 'clt_tasas';

    protected $fillable = [
    	'pais_origen_id', 'pais_destino_id', 'tasa', 'monto_min'
    ];

    public function _pais_origen ()
    {
    	return $this->belongsTo('App\Models\Cliente\Pais', 'pais_origen_id');
    }

    public function _pais_destino ()
    {
    	return $this->belongsTo('App\Models\Cliente\Pais', 'pais_destino_id');
    }
}
