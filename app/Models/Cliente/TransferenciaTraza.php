<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;

class TransferenciaTraza extends Model
{
    const UPDATED_AT = null;
    protected $table = 'clt_transferencias_trazas';

    protected $fillable = [
    	'id_estado', 'transferencia_id'
    ];

    public function _estado ()
    {
    	return $this->belongsTo('App\Models\Comun\Subtipo', 'id_estado');
    }

    public function _transferencia ()
    {
    	return $this->belongsTo('App\Models\Cliente\Transferencia', 'transferencia_id');
    }
}
