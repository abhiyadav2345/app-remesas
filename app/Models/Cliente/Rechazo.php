<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;

class Rechazo extends Model
{
	const UPDATED_AT = null;
    protected $table = 'clt_rechazos';

    protected $fillable = [
    	'id', 'transferencia_id', 'motivo', 'fecha_resolucion'
    ];

    public function _transferencia ()
    {
    	return $this->belongsTo('App\Models\Cliente\Transferencia', 'transferencia_id');
    }
}
