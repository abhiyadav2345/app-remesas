<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BancoTipoCuenta extends Model
{
    use SoftDeletes;

    protected $table = 'clt_tipos_cuentas';

    protected $fillable = [
	       'banco_id', 'id_tipo_cuenta',
    ];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function _banco()
    {
        return $this->belongsTo('App\Models\Cliente\Banco','banco_id');
    }

    public function _subtipo()
    {
        return $this->belongsTo('App\Models\Cliente\Banco','banco_id');
    }
}
