<?php

namespace App\Models\Cliente;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Usuario extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes;

    protected $table = 'clt_usuarios';

    protected $fillable = [
        'nombre', 'apellidos', 'cedula', 'email', 'avatar', 'pais_id', 'password', 'email_verified_at',
        'telefono', 'provider', 'provider_id', 'notificacion', 'activo', 'remember_token'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'nombre_completo'
    ];

    public function _pais ()
    {
        return $this->belongsTo('App\Models\Cliente\Pais', 'pais_id');
    }

    public function _transferencias ()
    {
        return $this->hasMany('App\Models\Cliente\Transferencia', 'usuario_id');
    }

    public function _cuentas ()
    {
        return $this->belongsTo('App\Models\Cliente\Cuenta', 'usuario_id');
    }

    public function _recovery(){

        return $this->hasOne(PasswordRecovery::class , 'usuario_id')->orderBy('id','DESC');
    }

    public function getNombreCompletoAttribute ()
    {
        return $this->nombre.' '.$this->apellidos;
    }
}
