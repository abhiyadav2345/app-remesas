	<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile text-center">
						<a href="javascript:;" data-toggle="nav-profile">
							<div class="cover with-shadow"></div>
							<div class="image" style="margin-left: 64px">
								@auth
									@if(Auth::user()->avatar)
										<img src="{{ Auth::user()->avatar }}" alt="" />
									@else
										<img src="{{asset('/images/users/002.jpg')}}" alt="" />
									@endif
								@endauth
							</div>
							<div class="info">
								@auth
									<b class="caret pull-right"></b>
									{{ Auth::user()->nombre .' '. Auth::user()->apellidos }}
									<small>{{ Auth::user()->_perfil ? Auth::user()->_perfil->nombre : 'Sin perfil'}}</small>
								@endif
							</div>
						</a>
					</li>
					<li>
						<ul class="nav nav-profile">
							<li><a href="javascript:;"><i class="fa fa-cog"></i> Configuración</a></li>
							<li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Recomendaciones</a></li>
							<li><a href="javascript:;"><i class="fa fa-question-circle"></i> Ayuda</a></li>
						</ul>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- MENU DE NAVEGACIÓN -->
				<ul class="nav">
					<li class="nav-header">Navegación</li>
					<li class="active">
						<a href="{{ route('home') }}">
							<i class="fa fa-th-large"></i>
							<span>Dashboard</span>
						</a>
					</li>
					<li class="has-sub">
							<a href="javascript:;">
								<b class="caret"></b>
								<i class="fas fa-desktop"></i>
								<span>Sistema</span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="{{url('sistema\administradores')}}">
										Administradores
										<i class="fas fa-user text-theme"></i>
									</a>
								</li>
								<li>
									<a href="{{url('sistema\secciones')}}">
										Secciones
										<i class="fas fa-list-alt text-theme"></i>
									</a>
								</li>
							</ul>
						</li>
					@auth
						<li class="has-sub">
							<a href="javascript:;">
								<b class="caret"></b>
								<i class="fas fa-gem"></i>
								<span>Mantenedores</span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="{{url('mantenedor\tipos')}}">
										Tipos
										<i class="fab fa-typo3 text-theme"></i>
									</a>
								</li>
								<li>
									<a href="{{url('mantenedor\paises')}}">
										Paises
										<i class="fa fa-flag text-theme"></i>
									</a>
								</li>
								<li>
									<a href="{{url('mantenedor\monedas')}}">
										Monedas
										<i class="far fa-money-bill-alt text-theme"></i>
									</a>
								</li>
								<li>
									<a href="{{url('mantenedor\tasas')}}">
										Tasas
										<i class="fas fa-exchange-alt text-theme"></i>
									</a>
								</li>
								<li>
									<a href="{{url('mantenedor\bancos')}}">
										Bancos
										<i class="fas fa-building text-theme"></i>
									</a>
								</li>
							</ul>
						</li>
						<li class="has-sub">
							<a href="javascript:;">
								<b class="caret"></b>
								<i class="fab fa-keycdn"></i>
								<span>Transferencias</span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="{{url('cliente\transferencias')}}">
										En proceso
										<i class="fab fa-keycdn text-theme"></i>
									</a>
								</li>
								<li>
									<a href="{{url('cliente\transferencias')}}">
										Finalizados
										<i class="fab fa-keycdn text-theme"></i>
									</a>
								</li>
								<li>
									<a href="{{url('cliente\transferencias')}}">
										Rechazados
										<i class="fab fa-keycdn text-theme"></i>
									</a>
								</li>
							</ul>
						</li>
							{{-- @foreach(Auth::user()->_secciones() as $seccion)
								<li class="has-sub">
									<a href="javascript:;">
										<b class="caret"></b>
										<i class="{{ $seccion->icono }}"></i>
										<span>{{ $seccion->nombre }}</span>
									</a>
									<ul class="sub-menu">
										@foreach(Auth::user()->_menus($seccion->id) as $menu)
											<li>
												<a href="{{ url($menu->ruta) }}">
													{{ $menu->nombre }}
													<i class="{{ $menu->icono }} text-theme"></i>
												</a>
											</li>
										@endforeach
									</ul>
								</li>
							@endforeach --}}
					@endauth
					<li>
						<a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify">
							<i class="fa fa-angle-double-left"></i>
						</a>
					</li>
					<!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
