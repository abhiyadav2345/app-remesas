const routes = [
	// ADMINISTRADORES
	{
		path: '/sistema/administradores',
		component: () => import(/* webpackChunkName: 'app/Sistema/Administradores/Index' */ '_app/Sistema/Administradores/Index'),
	},
	{
		path: '/sistema/administradores/crear',
		component: () => import(/* webpackChunkName: 'app/Sistema/Administradores/Crear' */ '_app/Sistema/Administradores/Crear'),
	},
	{
		path: '/sistema/administradores/:id/editar',
		component: () => import(/* webpackChunkName: 'app/Sistema/Administradores/Editar' */ '_app/Sistema/Administradores/Editar'),
	},
	{
		path: '/sistema/administradores/:id/permisos',
		component: () => import(/* webpackChunkName: 'app/Sistema/Administradores/Permisos' */ '_app/Sistema/Administradores/Permisos'),
	},

	// SECCIONES
	{
		path: '/sistema/secciones',
		component: () => import(/* webpackChunkName: 'app/Sistema/Secciones/Index' */ '_app/Sistema/Secciones/Index'),
	},
	{
		path: '/sistema/secciones/crear',
		component: () => import(/* webpackChunkName: 'app/Sistema/Secciones/Crear' */ '_app/Sistema/Secciones/Crear'),
	},
	{
		path: '/sistema/secciones/:id/editar',
		component: () => import(/* webpackChunkName: 'app/Sistema/Secciones/Editar' */ '_app/Sistema/Secciones/Editar'),
	},

	// MENUS
	{
		path: '/sistema/secciones/:idSeccion/menus',
		component: () => import(/* webpackChunkName: 'app/Sistema/Menus/Index' */ '_app/Sistema/Menus/Index'),
	},
	{
		path: '/sistema/secciones/:idSeccion/menus/crear',
		component: () => import(/* webpackChunkName: 'app/Sistema/Menus/Crear' */ '_app/Sistema/Menus/Crear'),
	},
	{
		path: '/sistema/secciones/:idSeccion/menus/:id/editar',
		component: () => import(/* webpackChunkName: 'app/Sistema/Menus/Editar' */ '_app/Sistema/Menus/Editar'),
	},

	// ACCIONES
	{
		path: '/sistema/secciones/:idSeccion/menus/:idMenu/acciones',
		component: () => import(/* webpackChunkName: 'app/Sistema/Acciones/Index' */ '_app/Sistema/Acciones/Index'),
	},
	{
		path: '/sistema/secciones/:idSeccion/menus/:idMenu/acciones/crear',
		component: () => import(/* webpackChunkName: 'app/Sistema/Acciones/Crear' */ '_app/Sistema/Acciones/Crear'),
	},
	{
		path: '/sistema/secciones/:idSeccion/menus/:idMenu/acciones/:id/editar',
		component: () => import(/* webpackChunkName: 'app/Sistema/Acciones/Editar' */ '_app/Sistema/Acciones/Editar'),
	}
];

export default routes;
