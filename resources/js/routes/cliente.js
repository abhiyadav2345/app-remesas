const routes = [
	// TRANSFERENCIAS
	{
		path: '/cliente/transferencias',
		component: () => import(/* webpackChunkName: 'app/Cliente/Transferencia/Index' */ '_app/Cliente/Transferencias/Index')
	},
];

export default routes;
